package site.assad.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2017/12/15 12:49
 * Description:
 */
public class UserServcieTest {

    private UserService userService;
    private final static Logger log = LogManager.getLogger();

    @Before
    public void initContext(){
        userService = new ClassPathXmlApplicationContext("classpath:applicationContext.xml").getBean("userService",UserService.class);
    }

    //测试缓存队列
    @Test
    public void testGetUser(){
        for(int i=1;i<5;i++){
            log.debug("query get User(userId=1)"+i+" ...");
            log.debug(userService.getUser(1));
        }
    }

    //测试删除缓存队列
    @Test
    public void testRemoveUserFromCache(){
        for(int i=1;i<5;i++){
            log.debug("query get User(userId=1)"+i+" ...");
            log.debug(userService.getUser(1));
        }
        userService.removeUserFromCache(1);
        log.debug("query get User(userId=1)"+6+" ...");
        log.debug(userService.getUser(1));
    }

}
