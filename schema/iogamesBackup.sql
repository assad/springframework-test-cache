-- MySQL dump 10.13  Distrib 5.5.53, for Win32 (AMD64)
--
-- Host: localhost    Database: iogames
-- ------------------------------------------------------
-- Server version	5.5.53

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `user_password` varchar(41) NOT NULL,
  `create_date` date DEFAULT NULL,
  `user_icon` varchar(20) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (32,'vancer','*98A2E16E81BE1B2EC03F18A391E9176A01C64698','2017-02-20','6'),(1,'assad','*BF4687677E37B3FDE0505974D9CA9782301EDC7F','2017-02-20','7'),(33,'微笑微笑','*A991D2FFD35DFAB7F21BFDDF38F5D429531A75AD','2017-02-20','3'),(34,'mape','*E54AD35A1DCBD34B147B75126046D5363986EB21','2017-02-20','7'),(35,'搞事搞事','*23AE809DDACAF96AF0FD78ED04B6A265E05AA257','2017-02-20','4'),(36,'assad1','*1D2774CC86CAEB74B16BFB0261677C34183332C0','2017-06-14','9'),(37,'Al-assad','123','2017-08-13','1'),(38,'ai','123','2017-09-04','1'),(41,'ADDTEST_0','*4CBBEE30863E7F3598917FD3A02AB2C69E1644D9','2017-12-14','7'),(42,'ADDTEST_1','*91FF9C6726CC758B782CFA32D9A35144712DD035','2017-12-14','8'),(43,'ADDTEST_2','*EB7B6CB088B88A482B3B8AD60B340EB745A16A6D','2017-12-14','7'),(44,'ADDTEST_3','*9E1DD5F65BA6BFC21C5F164082483CF0B971F8FD','2017-12-14','7'),(45,'ADDTEST_4','*1966A099C3323FE57D4F9A0C838CE33FFB3138A6','2017-12-14','5');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games_item`
--

DROP TABLE IF EXISTS `games_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games_item` (
  `game_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_name` varchar(25) NOT NULL,
  `game_link` varchar(40) NOT NULL,
  `game_description` text,
  `collected_count` int(11) DEFAULT '0',
  `liked_count` int(11) DEFAULT '0',
  `create_date` date DEFAULT NULL,
  PRIMARY KEY (`game_id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games_item`
--

LOCK TABLES `games_item` WRITE;
/*!40000 ALTER TABLE `games_item` DISABLE KEYS */;
INSERT INTO `games_item` VALUES (1,'hexagor.io','http://hexagor.io/','Once you have some land surrounding your base you can begin to build towers that can defend you in case of an intrusion by another player. Be careful because other players will try to break into your base and conquer it, if they do all of your land will be taken over and deleted! To be successful in the world of Hexagor.io I’d suggest trying out several builds and ensuring that the land surrounding your home base has a lot of turrets to defend against incoming attacks. When expanding its important to try and gather wood/stone plots early to get supplies for towers, to harvest the resources simply click on a claimed resource plot and it will harvest the supplies.\r\n',12,9,'2017-02-12'),(3,'krew.io','http://krew.io/','Point to move the ship \nLeft click to shoot \nRight click for speed boost \nAvoid asteroids and the borders \nBe the top player! ',18,14,'2017-02-12'),(4,'spheroids.xyz','http://spheroids.xyz/','Travel around space in true zero gravity like fashion, a small amount of boost can go a long way, possibly a bit too long as you can reach high speeds in no time making your sphere hard to control. The moons seem to have their own gravitational pull which can be used to stop your sphere and make it easy to shoot one target without flying off.',6,3,'2017-02-12'),(5,'occup.io','http://occup.io/','Click near your region to expand.\nDrag to move the viewport.\n points allow to occupy other regions.\nNo expansion in 3 sec.? GAME OVER',8,3,'2017-02-12'),(6,'wormate.io','https://wormate.io/','Wormate is basically a Slither styled game with a twist, this game features cool power ups, a lot more customisation and awesome food graphics that make them look extra tasty even to the pickiest of worms.',3,3,'2017-02-12'),(7,'deeeep.io','http://deeeep.io/','MOUSE to move\r\nCLICK to use boost (might miss if too close)\r\nENTER to chat (will reveal your position)\r\nM hide/show chat messages\r\n\r\nYou can hit every animal except of those of the same species (doesn\'t apply for sharks, they can eat other sharks too!)',6,1,'2017-02-12'),(8,'mope.io','http://mope.io/','Click (to run) and \'W\' (to swim/use ability)\r\n               \r\n\r\nAvoid players outlined RED (They can eat you)!\r\nPress ENTER in-game to chat\r\nEat players/food outlined LIGHT-GREEN!  ',4,3,'2017-02-12'),(9,'agar.io','http://agar.io/','In the world of Diep you control a tank and shoot projectiles who’s behaviour can be modified through the levelling system to defend yourself and attack opponents. Every 10 levels you will get the choice to modify your overall vehicle and change the way you shoot/play. Like several other io games there is an in game leader board which displays the players score and an icon of the type of tank they are currently using, I’d recommend that new players pay attention to this to begin with so you can get a better feel for strong tank builds. It is highly recommended that you try to move as much as possible as the higher level tanks deal a lot of damage to new players!',22,27,'2017-02-12'),(10,'bloble.io','http://bloble.io/','Bloble is an incredibly entertaining 2d strategy game, it pits you in a map with multiple other opponents dotted around the map and its your job to see that they fall. You are given a limited amount of space to build and develop a troop indicated by a dashed circle, within this space you can build 7 types of buildings and upgrade them through the cost of power.\r\n            \r\n            \r\nAll buildings can be placed or removed through left clicking, the WASD or arrow keys can be used to navigate the camera and the R key resets your camera back to your base view.',32,16,'2017-02-12'),(11,'myball.io','http://myball.io/','Control: Arrow or WASD keys\r\nShoot: Space bar\r\nBooster: Shift or Z or M key\r\nYour team is decided when you get into each game field (Left:Red Team, Right Blue Team). If both teams are full, game begins. Enjoy!',9,7,'2017-02-12'),(12,'ppep.io','http://ppep.io','Ppep.io is a really cool, pacman-style .io game! You collect mass as you would in agar.io, but your movements are restricted to up, down, left, right. Good luck!',4,10,'2017-02-12'),(13,'jomp.io','http://jomp.io/','The game itself is quite simple dispute being annoyingly tricky, your ball will continue to bounce as you move it however certain parts of terrain will cause your ball to bounce higher or lower. Like I already mentioned there are obstacles along the way which will end your run on contact such as spikes, lava and swinging balls, there may be more but I didn’t have much lucky advancing when I played :P.',3,11,'2017-02-12'),(14,'flar.io','http://www.flar.io/','You start off with a single base and a small sum of cash, aim to slowly but surely expand putting multiple bases on the map, adding quarries to farm rocks for you and defend your borders with heaps of turrets. The awesome thing about this game is that the map is fairly small which means a whole lot of action. Like all strategy games the match lengths depend on the players, a game can last the full duration of about 50 minutes on occasions or force-ably make all your opponents quit with your decision making prowess! I don’t play many strat games myself but Flario definitely made it easy to get in and enjoy within the first few minutes.',5,1,'2017-02-12'),(15,'snowfight.io','http://snowfight.io/',NULL,2,4,'2017-02-12'),(16,'slither.io','http://slither.io/',NULL,17,17,'2017-02-12'),(17,'karnage.io','http://karnage.io/',NULL,10,7,'2017-02-12'),(18,'oib.io','http://oib.io/',NULL,8,11,'2017-02-12'),(19,'forceship.eu','http://forceship.eu/',NULL,7,7,'2017-02-12'),(20,'skyarena.io','http://skyarena.io/',NULL,1,1,'2017-02-12'),(21,'narwhale.io','http://narwhale.io/',NULL,3,1,'2017-02-12'),(22,'limax.io','http://limax.io/',NULL,6,-1,'2017-02-12'),(23,'foodfight.ga','http://foodfight.ga/',NULL,15,8,'2017-02-12'),(24,'loomen.io','http://loomen.io/',NULL,10,11,'2017-02-12'),(25,'oceanar.io','http://oceanar.io/',NULL,6,2,'2017-02-12'),(26,'astrar.io','http://astrar.io/',NULL,18,14,'2017-02-12'),(27,'begods.online','https://begods.online/',NULL,8,11,'2017-02-12'),(28,'gravitygame.io','http://gravitygame.io/',NULL,7,3,'2017-02-12'),(29,'pikan.io','http://pikan.io/',NULL,14,16,'2017-02-12'),(30,'driftin.io','http://driftin.io/',NULL,5,5,'2017-02-12'),(31,'dstruct.io','http://dstruct.io/',NULL,7,3,'2017-02-12'),(32,'diep.io','https://diep.io/',NULL,3,2,'2017-02-12');
/*!40000 ALTER TABLE `games_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games_collect`
--

DROP TABLE IF EXISTS `games_collect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games_collect` (
  `game_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`game_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games_collect`
--

LOCK TABLES `games_collect` WRITE;
/*!40000 ALTER TABLE `games_collect` DISABLE KEYS */;
INSERT INTO `games_collect` VALUES (1,1),(1,35),(3,1),(3,32),(3,34),(4,1),(4,32),(4,35),(5,1),(5,33),(5,34),(5,36),(7,32),(7,34),(7,35),(8,1),(8,32),(8,34),(8,35),(9,1),(9,32),(9,34),(10,1),(10,33),(10,36),(11,1),(11,32),(12,1),(12,32),(13,1),(13,34),(14,32),(14,34),(14,35),(15,34),(16,34),(17,32),(18,32),(18,33),(18,35),(19,1),(19,32),(20,34),(21,32),(21,35),(21,36),(22,1),(22,32),(22,34),(22,35),(23,33),(23,35),(24,1),(24,34),(24,35),(24,36),(25,1),(25,36),(26,32),(26,33),(26,34),(27,1),(27,32),(27,34),(27,35),(28,1),(28,33),(29,32),(29,33),(29,34),(29,36),(30,34),(30,35),(31,33),(31,34),(32,32),(32,35),(32,36);
/*!40000 ALTER TABLE `games_collect` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-15 16:08:27
